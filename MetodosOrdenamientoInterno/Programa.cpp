#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include "Ordenamiento.h"

using namespace std;

void impresion(int* arreglo1, int* arreglo2, int* arreglo3, int* arreglo4 ,int* arreglo5 , int* arreglo6, int* arreglo7, Ordenamiento orden, int tam){
	cout << "Burbuja menor: ";
	orden.imprimir(arreglo1, tam);
	cout << "Burbuja mayor: ";
	orden.imprimir(arreglo2, tam);
	cout << "Insercion: ";
	orden.imprimir(arreglo3, tam);
	cout << "Insercion binaria: ";
	orden.imprimir(arreglo4, tam);
	cout << "Seleccion:";
	orden.imprimir(arreglo5, tam);
	cout << "Shell: ";
	orden.imprimir(arreglo6, tam);
	cout << "Quicksort: ";
	orden.imprimir(arreglo7, tam);

}

void llenado(Ordenamiento orden, int* arreglo, int tam, string p){
	/*Se rellena el arreglo aleatoriamente*/
	for(int i = 0; i < tam; i++){
		int numero_aleatorio = (rand() % tam) + 1;
		arreglo[i] = numero_aleatorio;
	}

	int *arreglo1 = new int[tam];
	int *arreglo2 = new int[tam];
	int *arreglo3 = new int[tam];
	int *arreglo4 = new int[tam];
	int *arreglo5 = new int[tam];
	int *arreglo6 = new int[tam];
	int *arreglo7 = new int[tam];
	/*se les asignan los mismos numeros a todos los arreglos*/
	arreglo1 = arreglo;
	arreglo2 = arreglo;
	arreglo3 = arreglo;
	arreglo4 = arreglo;
	arreglo5 = arreglo;
	arreglo6 = arreglo;
	arreglo7 = arreglo;

	cout << "Método             - Tiempo" << endl;
	orden.burbuja_menor(arreglo1, tam);
	orden.burbuja_mayor(arreglo2, tam);
	orden.insercion(arreglo3, tam);
	orden.insercion_binaria(arreglo4, tam);
	orden.seleccion(arreglo5, tam);
	orden.shell(arreglo6, tam);
	orden.quicksort(arreglo7, tam);
	cout << " " << endl;
	if (p == "s" ){
		impresion(arreglo1, arreglo2, arreglo3, arreglo4, arreglo5, arreglo6, arreglo7, orden, tam);
	}
}


int main(int argc, char *argv[]){
	/*Verifica la entrada de parámetros, si no hay se finaliza.*/
	if(argv[0] == NULL or argv[1] == NULL or argv[2] == 0){
		cout<< "No Ingresó Parámetros" << endl;
		return 0;
	}
	try{
		char n = stoi(argv[1]);
    }
    catch (const std::invalid_argument& e){
		cout << "El primer parámetro debe ser un número" << endl;
        exit(1);
    }

    Ordenamiento orden = Ordenamiento();
    int tam = stoi(argv[1]);
	/*validacion del parámetro numérico*/
    if(tam >= 1000000 or tam <= 0){
        cout << "El Número ingresado no es válido." << endl;
    }
    else {
		/*si el parámetro es válido, se crea el arreglo*/
		int *arreglo = new int[tam];
		string p = argv[2];
		llenado(orden, arreglo, tam, p);
    }
    return 0;
}

